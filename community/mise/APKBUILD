# Maintainer: Jeff Dickey <alpine@mise.jdx.dev>
pkgname=mise
pkgver=2024.8.15
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://mise.jdx.dev"
arch="all !s390x !riscv64 !ppc64le !loongarch64" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
subpackages="$pkgname-doc"
provides="rtx=$pkgver-r$pkgrel"
replaces="rtx"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdx/mise/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin mise
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/mise -t "$pkgdir/usr/bin/"
	install -Dm644 README.md docs/*.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm644 "man/man1/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
}

sha512sums="
27f9d44c455d2ee58bc296dcce5e6d77f3be3faf4e3ba3153380c638f5fcf170889bacfcbb6c98e4f2ce4d36ad2a9096783d7a8907fea8b2602d29d3db5dfd7e  mise-2024.8.15.tar.gz
"
