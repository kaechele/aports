# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadi-calendar-tools
pkgver=24.08.0
pkgrel=0
# ppc64le, s390x, riscv64, armhf and armv7 blocked by akonadi-calendar -> kmailtransport -> libkgapi -> qt6-qtwebengine
# loongarch64 blocked by calendarsupport
arch="all !armhf !ppc64le !s390x !riscv64 !armv7 !loongarch64"
url="https://kontact.kde.org/"
pkgdesc="CLI tools to manage akonadi calendars"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-calendar-dev
	akonadi-dev
	calendarsupport-dev
	extra-cmake-modules
	kcalendarcore-dev
	kcalutils-dev
	kdoctools-dev
	libkdepim-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/pim/akonadi-calendar-tools.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-calendar-tools-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a29abacbd77396ed2a9c7fa9e6104e28d6b2c47186e05c91ed8f32abc5451438b74c28ecbea157dcc686853a797b2fffc524857996613d4579d73f5c60070436  akonadi-calendar-tools-24.08.0.tar.xz
"
