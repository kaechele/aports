# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=keysmith
pkgver=24.08.0
pkgrel=0
pkgdesc="OTP client for Plasma Mobile and Desktop"
url="https://invent.kde.org/kde/keysmith"
arch="all !armhf"
license="GPL-3.0-or-later"
depends="
	kirigami
	kirigami-addons
	"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	ki18n-dev
	kirigami-dev
	libsodium-dev
	qqc2-desktop-style-dev
	qt6-qt5compat-dev
	qt6-qtbase-dev
	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/keysmith.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/keysmith-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -E "account-storage-(object-lifecycles|default-lifecycle|hotp-counter-update)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
10016b4d3268e5b7caa52765602ffbc2c4b7d66f2c1ff6053110781b64981f304198fc8351889251c4c32b3647648af67389d440ed625d592ada80080d0070d8  keysmith-24.08.0.tar.xz
"
