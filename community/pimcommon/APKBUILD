# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=pimcommon
pkgver=24.08.0
pkgrel=0
pkgdesc="Common lib for KDEPim"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt6-qtwebengine -> akonadi
# loongarch64 blocked by purpose
arch="all !armhf !ppc64le !s390x !riscv64 !loongarch64"
url='https://community.kde.org/KDE_PIM'
license="GPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev
	akonadi-dev
	karchive-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kimap-dev
	kio-dev
	kitemmodels-dev
	kjobwidgets-dev
	kmime-dev
	knewstuff-dev
	kpimtextedit-dev
	kservice-dev
	ktextaddons-dev
	ktexttemplate-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdepim-dev
	purpose-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt6-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/pimcommon.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/pimcommon-$pkgver.tar.xz"
options="net" # net required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E "pimcommon-needupdateversion-needupdateversionwidgettest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
5907285f4937b82be5567083b20acdbd7e6d7ae31a3e072e5c3c0a7b8ce28cba979d74990afc691e336e244aaf06e92eb806d53c770c2149ca507bba08269e17  pimcommon-24.08.0.tar.xz
"
