# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadi-mime
pkgver=24.08.0
pkgrel=0
pkgdesc="Libraries and daemons to implement basic email handling"
# s390x, riscv64 and loongarch64 blocked by akonadi
# ppc64le, armhf blocked by qt6-qtwebengine -> kaccounts-integration
arch="all !armhf !s390x !riscv64 !ppc64le !loongarch64"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	akonadi-dev
	kcodecs-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	kitemmodels-dev
	kmime-dev
	kxmlgui-dev
	libxslt-dev
	qt6-qtbase-dev
	shared-mime-info
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/akonadi-mime.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-mime-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# mailserializerplugintest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "mailserializerplugintest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
e8cc8c420423cf76e5195f02e0d52b0af339ebdbf9bd38ccf39f482e5deb313164dfe5bc631c2fe8beec7a6aaf0798f8e04987db83509fda230dc91aba4f1c09  akonadi-mime-24.08.0.tar.xz
"
