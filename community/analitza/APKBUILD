# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=analitza
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/"
pkgdesc="A library to add mathematical features to your program"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends_dev="
	eigen-dev
	qt6-qt5compat-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtsvg-dev
	qt6-qttools-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/education/analitza.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/analitza-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E "(analitza|planecurve|plotsmodel)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f72f2e4c19ea4566a3a1f383e821835ef829c829cee6c65f593b763274cf583e919c93a5b77d1ff95303126c292a564a0275e4ee4cecd9c42a93a05c851b843b  analitza-24.08.0.tar.xz
"
